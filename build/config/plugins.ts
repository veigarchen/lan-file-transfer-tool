import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'
import { UIViteAutoImport } from 'ui-vite/src/autoimport'
import { PluginOption } from 'vite'
import { viteVar, viteDef } from 'vite-var'
import { globalType } from '../env/globalVar'
export const getPlugins = (env: globalType) => {
    const isBuild = env.env.pro === 'build'
    const plugin: PluginOption[] = [
        viteVar(env),
        viteDef(env.env.pro),
        UIViteAutoImport({
            isBuild: isBuild,
            include: ['VSwitch']
        }),
        vue({
            template: {
                compilerOptions: {
                    isCustomElement: (tag) => tag.startsWith('lottie-') || tag.startsWith('marquee')
                }
            }
        }),
        visualizer({
            open: env.env.BUILDVIEW,
            gzipSize: true,
            brotliSize: true
        }) as any
    ]
    return plugin
}
