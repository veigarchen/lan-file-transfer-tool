import { resolve, relative, dirname } from 'path'
import FileUtil from './src/utils/FileUtil'

const init = async () => {
    const ctlsPath = resolve('./src/Ctls.ts')
    let importStr = ''
    let exportStr = ''
    await FileUtil.readDirectory(resolve('./src'), (filepath: string) => {
        const regex = /.*\\(.*Controller)\.ts$/i
        const result = regex.exec(filepath)
        if (result && result[1]) {
            const appControllerPath = filepath
            let relativePath = relative(dirname(ctlsPath), appControllerPath)
            relativePath = './' + relativePath.replace(/\\/g, '/').replace(/\.ts$/, '')
            importStr += `import ${result[1]} from '${relativePath}'\n`
            exportStr += ` ${result[1]},`
        }
    })

    exportStr = `export default {${exportStr}}`

    await FileUtil.writeFile(ctlsPath, importStr + exportStr)
}

init()
