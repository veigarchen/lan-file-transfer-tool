import * as multer from 'multer'
import SData from './SData'

const storage = multer.diskStorage({
    destination: SData.dir,
    filename: function (req, file, cb) {
        file.originalname = Buffer.from(file.originalname, 'latin1').toString('utf8')
        let fileName = decodeURI(file.originalname)
        console.log('fileName', fileName)

        fileName = SData.insert(fileName)
        cb(null, fileName)
    }
})

const upload = multer({ storage: storage })

export default class UploadUtil {
    static upload(req: any, res: any) {
        return new Promise((_pres) => {
            upload.any()(req, res, (err) => {
                if (err) {
                    _pres(false)
                } else {
                    _pres(true)
                }
            })
        })
    }
}
