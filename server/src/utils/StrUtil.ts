export default class StrUtil {
    /**
     * 获取uuid
     * @returns
     */
    static uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == 'x' ? r : (r & 0x3) | 0x8
            return v.toString(16)
        })
    }

    private static id = 1000
    /**
     * 获取唯一递增id
     */
    static getId = () => {
        this.id++
        return this.id + ''
    }

    /**
     * 判断内容是否为空,null,undefined,NaN，空数组，空字符串返回true
     */
    static isNull(str: any): boolean {
        if (Array.isArray(str)) return str.length == 0
        if (str + '' !== 'null' && str + '' !== 'undefined' && str !== '' && str + '' !== 'NaN') return false
        return true
    }

    /**
     * 验证邮箱
     */
    static isEmail(obj: any): boolean {
        var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/
        if (!myreg.test(obj)) return false
        return true
    }

    /**
     * 验证数字
     */
    static isNumber(obj: any): boolean {
        return /^[0-9]+$/.test(obj)
    }

    /**
     * 获取url地址中的参数对象信息
     * @param url 如/list?pageSize=1&pageNum=2
     * @param return 如{pageSize:1,pageNum:2}
     */
    static getParam(url: any): any {
        const regex = /[?&]([^=#]+)=([^&#]*)/g
        if (url.match) {
            const matchVal = url.match(regex)
            if (matchVal)
                return matchVal.reduce((acc: any, cur: any) => {
                    const [param, value] = cur.substring(1).split('=')
                    acc[param] = decodeURIComponent(value)
                    return acc
                }, {})
        }
        return {}
    }
}
