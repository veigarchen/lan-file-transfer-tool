import * as fs from 'fs'
import * as path from 'path'

export default class FileUtil {
    static copyFile(source: string, destination: string): Promise<void> {
        return new Promise((resolve, reject) => {
            const rd = fs.createReadStream(source)
            rd.on('error', (err) => {
                reject(err)
            })

            const wr = fs.createWriteStream(destination)
            wr.on('error', (err) => {
                reject(err)
            })
            wr.on('close', () => {
                resolve()
            })

            rd.pipe(wr)
        })
    }

    static async deleteFileOrDirectory(filePath: string): Promise<void> {
        try {
            const stats = await fs.promises.lstat(filePath)
            if (stats.isDirectory()) {
                const files = await fs.promises.readdir(filePath)
                const promises = files.map((file) => FileUtil.deleteFileOrDirectory(path.join(filePath, file)))
                await Promise.all(promises)
                await fs.promises.rmdir(filePath)
            } else {
                await fs.promises.unlink(filePath)
            }
        } catch (err) {
            throw err
        }
    }

    static readFile(filePath: string, nobuffer: any = 'utf8'): Promise<any> {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, nobuffer, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data as any)
                }
            })
        })
    }

    static async readDirectory(directoryPath: string, fileCallback?: (filePath: string) => void): Promise<void> {
        try {
            const files = await fs.promises.readdir(directoryPath)
            const promises = files.map(async (file) => {
                const filePath = path.join(directoryPath, file)
                const stats = await fs.promises.stat(filePath)
                if (stats.isDirectory()) {
                    await FileUtil.readDirectory(filePath, fileCallback)
                } else {
                    if (fileCallback) {
                        fileCallback(filePath)
                    }
                }
            })
            await Promise.all(promises)
        } catch (err) {
            throw err
        }
    }

    static async writeFile(filePath: string, content: any, options: fs.WriteFileOptions = 'utf8'): Promise<void> {
        const directoryPath = path.dirname(filePath)
        try {
            fs.mkdirSync(directoryPath, { recursive: true })

            function logObjectType(obj: any) {
                const objectType = Object.prototype.toString.call(obj) as any
                return objectType.match(/\[object (\w+)\]/)[1]
            }

            if (typeof content !== 'string') {
                content = Buffer.from(content)
            }
            fs.writeFileSync(filePath, content, options)
        } catch {
            console.log('保存错误', filePath)
        }
    }
}
