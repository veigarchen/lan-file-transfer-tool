import 'reflect-metadata'

export const METHOD_METADATA = 'method'
export const PATH_METADATA = 'path'

export const GET = (path: string): MethodDecorator => {
    return (target, propertyKey: string | symbol, descriptor: PropertyDescriptor) => {
        Reflect.defineMetadata(PATH_METADATA, path, target, propertyKey)
        Reflect.defineMetadata(METHOD_METADATA, RequestMethod.GET, target, propertyKey)
    }
}

export const POST = (path: string): MethodDecorator => {
    return (target, propertyKey: string | symbol, descriptor: PropertyDescriptor) => {
        Reflect.defineMetadata(PATH_METADATA, path, target, propertyKey)
        Reflect.defineMetadata(METHOD_METADATA, RequestMethod.POST, target, propertyKey)
    }
}

export const Controller = (path?: string): ClassDecorator => {
    return (target) => {
        Reflect.defineMetadata(PATH_METADATA, path, target)
    }
}

export const RequestMethod = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    DELETE: 'delete',
    ALL: 'all'
}
