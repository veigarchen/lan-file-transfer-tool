export default class Res {
    status: boolean = true
    data: any = null
    message: any
    code: number = 200

    static success(...params: any) {
        const data = params[0]
        const message = params[1] ?? ''
        const res = new Res()
        res.data = data
        res.message = message
        return res.toJSON()
    }

    static fail(...params: any) {
        const message = params[0] ?? ''
        const code = params[1] ?? 500
        const res = new Res()
        res.message = message
        res.code = code
        return res.toJSON()
    }

    toJSON() {
        return JSON.stringify({
            status: this.status,
            data: this.data,
            message: this.message,
            code: this.code
        })
    }
}
