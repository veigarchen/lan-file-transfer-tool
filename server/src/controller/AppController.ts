import { Controller, GET, POST } from '../decorators'
import Res from '../model/Res'
import SData from '../utils/SData'
import UploadUtil from '../utils/UploadUtil'
@Controller('/app')
export default class AppController {
    @POST('/getConfig')
    getConfig(): string {
        return Res.success(SData.tempList)
    }

    @POST('/saveFile')
    async saveFile(data: any, req: any, res: any): Promise<any> {
        const contentType = req.headers['content-type']
        if (contentType && contentType.indexOf('multipart/form-data') !== -1) {
            const _res = await UploadUtil.upload(req, res)
            if (_res) {
                return Res.success('上传成功')
            } else {
                return Res.fail('上传失败')
            }
        } else {
            return Res.success('上传成功')
        }
    }
}
