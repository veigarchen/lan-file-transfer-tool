import { App } from 'vue'

import System from '~/utils/System'
import { intersection } from './intersection'

export const install = (app: App<Element>) => {
    app.directive('auth', {
        mounted(el: HTMLDivElement, binding) {
            if (!System.store.sys.auth.isPass(binding.value)) el.parentElement?.removeChild(el)
        }
    })
    intersection(app)
}

export default {
    install
}
