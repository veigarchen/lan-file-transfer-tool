import { createApp } from 'vue'
import 'tools-javascript'
import App from './App.vue'
import localcom from './components'
import { router } from './router'
import './styles/index.less'
import 'tools-css/index.css'
import Config from './utils/Config'

const app = createApp(App)
app.use(localcom).use(router)

import uivite from 'ui-vite'
app.use(uivite)

Config.init(app)
