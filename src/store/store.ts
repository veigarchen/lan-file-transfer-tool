import { reactive } from 'vue'
import conf from './conf'
import sys from './sys'
import user from './user'

export default () => {
    return reactive({
        //静态配置
        conf: conf(),

        sys: sys(),

        user: user()
    })
}
