import { reactive } from 'vue'

export default () => {
    /**
     * 用户信息
     */
    const user = reactive({
        token: '',
        text: '内容',
        setText: (text: string) => {
            user.text = text
        }
    })
    return user
}
