export default [
    {
        name: 'home',
        path: '/home',
        meta: {
            title: '首页',
            show: true
        },
        component: () => import('~/views/home/index.vue')
    },
    {
        name: 'login',
        path: '/login',
        meta: {
            title: '登录',
            show: true
        },
        component: () => import('~/views/login/index.vue')
    }
]
