import home from './home'

export default [
    {
        path: '/',
        redirect: '/home'
    },
    ...home
]
