export default class ApiUser {
    /** 登录验证 */
    static loginPc(UserName: string, Password: string) {
        let param = {
            UserName,
            Password
        }
        return http.post('/User/loginPc', param)
    }
}
