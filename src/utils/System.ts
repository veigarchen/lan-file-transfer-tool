import store from '~/store/store'
import { Router } from '~/vite-env'

export default class System {
    /**
     * 缓存
     */
    static store = store()

    /**
     * 路由
     */
    static router: Router

    //静态资源存放路径 如/static_admin
    static static: string = '/#{static}'
}
