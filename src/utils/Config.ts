import Theme from '~/styles/Theme'
import { envType } from '../../build/env/globalVar'

export default class Config {
    /**
     * 环境变量
     */
    static env: envType = {} as any
    static outtime = 60000

    static init = async (app: any) => {
        Config.env = JSON.parse('#{env}')
        Theme.init()
        await Config.getConfFile()
        app.mount('#app')
    }

    /**
     * http请求地址
     */
    static api: string = '/'
    static apiName = 'API'
    /**
     * websocket请求地址
     */
    static ws: string = '/'
    static wsName = 'WS'

    static config: any = {}

    //获取配置数据
    static getConfFile = () => {
        const env: any = Config.env
        Config.api = env[Config.apiName] ?? Config.api
        Config.ws = env[Config.wsName] ?? Config.api

        return new Promise(async (pres) => {
            pres(true)
        })
    }
}
