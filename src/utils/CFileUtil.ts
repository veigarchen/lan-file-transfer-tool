import { FileUtil } from 'tools-vue3'
export default class CFileUtil {
    /**
     *  下载文件
     */
    static async download(content: string, name: string = 'download') {
        const _res = await FileUtil.getFile(content, {
            responseType: 'blob'
        })

        const blobUrl = URL.createObjectURL(_res)

        var tempLink = document.createElement('a')
        tempLink.style.display = 'none'
        tempLink.href = blobUrl
        tempLink.setAttribute('download', name)
        if (typeof tempLink.download === 'undefined') {
            tempLink.setAttribute('target', '_blank')
        }
        document.body.appendChild(tempLink)
        tempLink.click()
        document.body.removeChild(tempLink)
    }
}
